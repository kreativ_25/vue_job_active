export default {
    data() {
        return {
          title: 'Hello i am Vue!!!',
          names: ['Alex', 'Bob', 'Aliaksander'],
          searchName: ''
        }
      },
      filters:{
        //Фильтры - это функции! :)
        lowercase(value){
          return value.toLowerCase()
        }
      },
      computed: {
        filtredNames(){
          return this.names.filter(name =>{
            return name.toLowerCase().indexOf(this.searchName.toLowerCase()) !== -1
          })
        }
      }
}