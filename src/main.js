import Vue from 'vue'
import App from './App.vue'
import Car from './Car.vue' //импортируем созданный Car.vue
import Job from './Job.vue'
import Counter from './Counter.vue'
import ColorDirective from './Color'
import List from './List.vue'

Vue.filter('upperCase', (value) => {
  return value.toUpperCase()
})

Vue.directive('colored', ColorDirective)

export const eventEmitter = new Vue()
//регистрация Car.vue
Vue.component('app-car', Car);
Vue.component('app-job', Job);
Vue.component('app-counter', Counter)
Vue.component('app-list', List)


new Vue({
  el: '#app',
  render: function(h){
    return h(App);
  }
})

export const eventEmiter = new Vue()